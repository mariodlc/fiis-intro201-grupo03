from tkinter import *
from tkinter import ttk
from tkinter import messagebox
ventana=Tk()
ventana.state(newstate="normal")
ventana.title("Menu Principal")
ventana.geometry("250x220")
alumnos=[]
cursos = ["Algebra", "Quimica", "Geometria", "Fisica"]
creditos =[5, 5, 3, 5]
horas_sem = [4, 5, 6, 6]
precio=[100, 125, 90, 150]
vacantes=[2, 45, 7, 10]
def modificar(l,h):    
    lista = []
    for i in range(len(l)):
        if(i == h):
            pass
        else:
            lista.append(l[i])
    l = lista
    return l
def visible1(con,bot):
    if (con.cget("show"))=="*":
        con.config(show="")
        bot.config(text="Ocultar")
    elif (con.cget("show"))=="":
        con.config(show="*")
        bot.config(text="Mostar")
def regresar(x):
    ventana.state(newstate="normal")
    x.state(newstate="withdrawn")
    for a in(x.winfo_children()):
        a.destroy()
def abrir1(x):
    alum=[]
    def verificar():
        continuar=True
        if nombre.get()=="" or apellido.get()=="" or edad.get()==0 or selec.get()=="" or selec.get()==0:
            messagebox.showerror("Error","Llene todos los campos")
            continuar=False
        if continuar==True:
            siguiente1()
    def siguiente1():
        def siguiente2():
            def veri():
                def boleta():
                    costos=[]
                    costo = 0
                    for w in range(len(cursos)):
                        for f in range(len(L)):
                            if(cursos[w] == L[f]):
                                saldo = precio[w]
                                costos.append(precio[w])
                                costo = costo+saldo
                    ventana6.state(newstate="normal")
                    ventana5.state(newstate="withdrawn")
                    fac1=Label(ventana6,text="Nombre:   "+nombre.get()).place(x=10,y=10)
                    fac2=Label(ventana6,text="Apellido:   "+apellido.get()).place(x=10,y=35)
                    fac6=Label(ventana6,text="Fecha:   28/09/2020").place(x=10,y=60)
                    fac4=Label(ventana6,text="Cursos:").place(x=10,y=85)
                    num=0
                    for i in range(len(L)):
                        fac9=Label(ventana6,text=L[i]).place(x=50,y=110+num)
                        fac10=Label(ventana6,text=str(costos[i])).place(x=150,y=110+num)
                        num=num+25
                        z=110+num
                    fac8=Label(ventana6,text="-"*25).place(x=50,y=z)
                    fac3=Label(ventana6,text="Costo total:").place(x=50,y=z+20)
                    fac7=Label(ventana6,text=str(costo)).place(x=150, y=z+20)
                    fac5=Button(ventana6,text="Aceptar",command=lambda:regresar(ventana6)).place(x=120,y=z+50)
                continuar=True
                if contra.get()=="":
                        messagebox.showerror("Error", "Ingrese una contrasena")
                        continuar=False
                for p in range(len(alumnos)):
                    if contra.get()==alumnos[p][4]:
                        messagebox.showerror("Error", "Ingrese otra contrasena")
                        continuar=False
                if continuar==True:
                    if selec.get()==1:
                        genero="Mujer"
                    elif selec.get()==2:
                        genero="Hombre"
                    alum=[]
                    alum.append(nombre.get())
                    alum.append(apellido.get())
                    alum.append(edad.get())
                    alum.append(genero)
                    alum.append(contra.get())
                    alum.append(L)
                    alumnos.append(alum)
                    boleta()
            ventana5.state(newstate="normal")
            ventana4.state(newstate="withdrawn")
            contra=StringVar()
            lcon=Label(ventana5,text="Ingrese una contrasena para su cuenta").place(x=10,y=10)
            nombreCaja5=Entry(ventana5,show="*",textvariable=contra)
            nombreCaja5.place(x=20,y=50)
            bu7=Button(ventana5,text="Mostar",command=lambda:visible1(nombreCaja5, bu7))
            bu7.place(x=170,y=48)
            bu5=Button(ventana5,text="Siguiente",command=lambda:[veri()]).place(x=170,y=100)
        ventana4.state(newstate="normal")
        ventana4.geometry("280x200")
        ventana2.state(newstate="withdrawn")
        l=[]
        rel=[0,0,0,0]
        for r in range(len(cursos)):
            cont=0
            for j in range(len(alumnos)):
                for k in range(len(alumnos[j][5])):
                    if(cursos[r] == alumnos[j][5][k]):
                        cont=cont+1
            rel[r]=rel[r]+cont
        for o in range(4):
            vacan=vacantes[o]-rel[o]
            l.append(vacan)
        C=[]
        for u in cursos:
            C.append(u)
        for y in range(len(l)):
            if(l[y] == 0):
                C[y]=0
        p=[]
        for e in C:
            if e!=0:
                p.append(e)
        L=[]
        selal=IntVar()
        selqui=IntVar()
        selgeo=IntVar()
        selfi=IntVar()
        na=0
        men=Label(ventana4,text="Seleccione los cursos que desea matricularse").place(x=10,y=10)
        for z in range(len(p)):
            if p[z]=="Algebra":
                bal=Checkbutton(ventana4,text=p[z],variable=selal,onvalue=1,offvalue=0).place(x=30,y=40+na)
            elif p[z]=="Quimica":
                bqu=Checkbutton(ventana4,text=p[z],variable=selqui,onvalue=1,offvalue=0).place(x=30,y=40+na)
            elif p[z]=="Geometria":
                bge=Checkbutton(ventana4,text=p[z],variable=selgeo,onvalue=1,offvalue=0).place(x=30,y=40+na)
            elif p[z]=="Fisica":
                bfi=Checkbutton(ventana4,text=p[z],variable=selfi,onvalue=1,offvalue=0).place(x=30,y=40+na)
            na=na+30
        def verificar2():
            continuar=True
            if selal.get()==1:
                L.append("Algebra")
            if selqui.get()==1:
                L.append("Quimica")
            if selgeo.get()==1:
                L.append("Geometria")
            if selfi.get()==1:
                L.append("Fisica")
            if L==[]:
                messagebox.showerror("Error","Seleccione algun curso.")
                continuar=False
            if continuar==True:
                for a in(ventana4.winfo_children()):
                    a.destroy()
                siguiente2()
        bu=Button(ventana4,text="Siguiente",command=lambda:[verificar2()]).place(x=200,y=150)
    x.state(newstate="normal")
    ventana.state(newstate="withdrawn")
    x.geometry("300x200")
    nombre=StringVar()
    apellido=StringVar()
    edad=IntVar()
    selec=IntVar()
    etiqueta1=Label(x,text="Nombre: ").place(x=10,y=10)
    nombreCaja1=Entry(x, textvariable=nombre).place(x=170,y=10)
    etiqueta2=Label(x,text="Apellido: ").place(x=10,y=40)
    nombreCaja2=Entry(x, textvariable=apellido).place(x=170,y=40)
    etiqueta3=Label(x,text="Edad: ").place(x=10,y=70)
    nombreCaja3=Entry(x, textvariable=edad).place(x=170,y=70)
    etiqueta4=Label(x,text="Genero").place(x=10,y=100)
    RB4=Radiobutton(x,text="Mujer",value=1,variable=selec).place(x=120,y=100)
    RB5=Radiobutton(x,text="Hombre",value=2,variable=selec).place(x=200,y=100)
    boton4=Button(x,text="Siguiente",command=lambda:[verificar()]).place(x=220,y=150)
def abrir2(x):
    def borrar():
        if len(alumnos)==0:
            messagebox.showerror("ERROR", "No hay alumnos incritos")
            regresar(ventana3)
        else:
            continuar=False
            for i in range(len(alumnos)):
                if(alumnos[i][0] == nombre2.get() and alumnos[i][4] == contrasena2.get()):
                    continuar=True
                    break
            if continuar==True:
                    alumnos.pop(i)
                    messagebox.showinfo("Proceso Completado", "Matricula cancelada")
                    regresar(ventana3)
            elif continuar==False:
                    messagebox.showerror("ERROR", "Usuario y/o contrasena incorrecto(s)")
    x.state(newstate="normal")
    ventana.state(newstate="withdrawn")
    x.geometry("350x150")
    nombre2=StringVar()
    contrasena2=StringVar()
    canom=Label(x,text="Nombre: ",).place(x=10,y=20)
    nomcan=Entry(x,textvariable=nombre2).place(x=150,y=20)
    cacon=Label(x,text="Contrasena: ").place(x=10,y=50)
    conca=Entry(x,show="*",textvariable=contrasena2)
    conca.place(x=150,y=50)
    canbu=Button(x,text="Siguiente",command=lambda:borrar()).place(x=280,y=100)
    canbu2=Button(x,text="Mostrar",command=lambda:visible1(conca,canbu2))
    canbu2.place(x=284,y=48)
    canbu1=Button(x,text="Regresar",command=lambda:regresar(ventana3)).place(x=10,y=100)
def abrir3(x):
    def siguiente4():
        if len(alumnos)==0:
            messagebox.showerror("ERROR", "No hay alumnos inscritos")
            regresar(ventana7)
        else:
            continuar=False
            for i in range(len(alumnos)):
                if(alumnos[i][0] == nombre3.get() and alumnos[i][4] == contrasena3.get()):
                    continuar=True
                    break
            if continuar==True:
                ventana8.geometry("180x220")
                ventana8.state(newstate="normal")
                ventana7.state(newstate="withdrawn")
                rnom=Label(ventana8,text="Nombre:  "+alumnos[i][0]).place(x=10,y=10)
                rap=Label(ventana8,text="Apellido:  "+alumnos[i][1]).place(x=10,y=40)
                rgen=Label(ventana8,text="Genero:    "+alumnos[i][3]).place(x=10,y=70)
                reda=Label(ventana8,text="Edad:        "+str(alumnos[i][2])).place(x=10,y=100)
                rcur=Label(ventana8,text="Cursos Matriculados: ").place(x=10,y=130)
                ni=0
                for j in range(len(alumnos[i][5])):
                    for k in range(len(cursos)):
                        if(cursos[k] == alumnos[i][5][j]):
                            cur=Label(ventana8,text=str(j+1)+"."+cursos[k]+".").place(x=40,y=160+ni)
                            cur2=Label(ventana8,text="Horas semanales: "+str(horas_sem[k])).place(x=50,y=190+ni)
                            ni=ni+60
                ventana8.geometry(newGeometry=("180x"+str(220+ni)))
                rebu=Button(ventana8,text="Aceptar",command=lambda:regresar(ventana8)).place(x=120,y=180+ni)
            else:
               messagebox.showerror("ERROR", "Usuario y/o contrasena incorrecto(s)")
    x.state(newstate="normal")
    ventana.state(newstate="withdrawn")
    x.geometry("350x150")
    nombre3=StringVar()
    contrasena3=StringVar()
    renom=Label(x,text="Nombre: ",).place(x=10,y=20)
    nomre=Entry(x,textvariable=nombre3).place(x=150,y=20)
    recon=Label(x,text="Contrasena: ").place(x=10,y=50)
    conre=Entry(x,show="*",textvariable=contrasena3)
    conre.place(x=150,y=50)
    rebu=Button(x,text="Siguiente",command=lambda:siguiente4()).place(x=280,y=100)
    repa=Button(x,text="Mostrar",command=lambda:[visible1(conre,repa)])
    repa.place(x=290,y=48)
    rebu1=Button(x,text="Regresar",command=lambda:regresar(ventana7)).place(x=10,y=100)
def abrir4(x):
    def siguiente6():
        if valis.get()=="":
            pass
        else:
            ventana10.state(newstate="normal")
            ventana9.state(newstate="withdrawn")
            sigv=Label(ventana10,text="Lista de alumnos:").place(x=10,y=10)
            cont=0
            nu=0
            for j in range(len(alumnos)):
                for k in range(len(alumnos[j][5])):
                    if(valis.get()) == (alumnos[j][5][k]):
                        cont=cont+1
                        siv=Label(ventana10,text=str(cont)+"."+alumnos[j][0]).place(x=10,y=30+nu)
                        nu=nu+30
                        ventana10.geometry(newGeometry="250x"+str(100+nu))
            if nu==0:
                alo=Label(ventana10,text="No hay alumnos inscritos en este curso").place(x=10,y=30+nu)
                nu=nu+30
            for i in range(len(cursos)):
                if cursos[i]==valis.get():
                    vacan1=vacantes[i]-cont
            siv2=Label(ventana10,text="Numero de vacantes: "+str(vacan1)).place(x=10,y=30+nu)
            boton7=Button(ventana10,text="Aceptar",command=lambda:regresar(ventana10)).place(x=180,y=60)
    x.state(newstate="normal")
    ventana.state(newstate="withdrawn")
    vala=Label(x,text="Seleccione un curso.").place(x=10,y=10)
    valis=ttk.Combobox(x,width=17)
    valis.place(x=10,y=30)
    valis["values"]=cursos
    boton5=Button(x,text="Siguiente",command=lambda:siguiente6()).place(x=135,y=70)
    boton7=Button(x,text="Regresar",command=lambda:regresar(x)).place(x=10,y=70)
ventana2=Toplevel()
ventana2.title("Ingreso de datos")
ventana2.state(newstate="withdrawn")
ventana3=Toplevel()
ventana3.title("Cancelar Matricula")
ventana3.state(newstate="withdrawn")
ventana4=Toplevel()
ventana4.title("Seleccion de cursos")
ventana4.state(newstate="withdrawn")
ventana5=Toplevel()
ventana5.title("Creacion de contrasena")
ventana5.state(newstate="withdrawn")
ventana5.geometry("250x150")
ventana6=Toplevel()
ventana6.title("Boleta")
ventana6.state(newstate="withdrawn")
ventana6.geometry("200x300")
ventana7=Toplevel()
ventana7.title("Registro de alumnos")
ventana7.geometry("300x300")
ventana7.state(newstate="withdrawn")
ventana8=Toplevel()
ventana8.title("Informacion del alumno")
ventana8.geometry("250x450")
ventana8.state(newstate="withdrawn")
ventana9=Toplevel()
ventana9.title("Vacantes")
ventana9.geometry("200x100")
ventana9.state(newstate="withdrawn")
ventana10=Toplevel()
ventana10.title("Vacantes")
ventana10.geometry("250x100")
ventana10.state(newstate="withdrawn")
boton2=Button(ventana,text="Matricula",width=14,command= lambda:abrir1(ventana2)).place(x=70,y=20)
boton3=Button(ventana,text="Cancelar matricula",command= lambda:abrir2(ventana3)).place(x=70,y=60)
boton4=Button(ventana,text="Buscar alumno",width=14,command=lambda:abrir3(ventana7)).place(x=70,y=100)
boton5=Button(ventana,text="Vacantes",width=14,command= lambda:abrir4(ventana9)).place(x=70,y=140)
boton6=Button(ventana,text="Salir",width=14,command= lambda:ventana.destroy()).place(x=70,y=180)

ventana.mainloop()
