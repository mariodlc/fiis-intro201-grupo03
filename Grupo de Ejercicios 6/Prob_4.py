def factorial(n):
    if n==0 or n==1:
        P=1
    else:
        P=1
        for i in range(1,n+1):
            P=P*i
    return P
    

def numero_de_divisores(n):
    contador=1
    for i in range (1,n//2+1):
        if(n%i==0):
            contador=contador+1
    return contador

def suma(n):
    S=0
    a=0
    i=0
    while(a!=n):
        i=i+1
        if (numero_de_divisores(i)==2):
            a=a+1
            
            S=S+(1/factorial(i))
    return S

n=int(input("ingrese un valor:"))

if (n<=0):
    print("ingrese un valor mayor a 0")

else:
    Suma=suma(n)
    
    print("El resultado es:", Suma)