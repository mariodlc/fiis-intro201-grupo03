def maximo(lista):
    mayor=None
    for x in range(len(lista)):
        if(mayor == None or lista[x] > mayor):
            mayor=lista[x]
    return mayor
def mineral_extraido(ciudad,n,m):
    lista=[]
    for i in range(len(ciudad)):
        for j in range(len(ciudad[0])):
            a = ciudad[n][m]
            if(n-1 == i and m == j):
                mineral= a + ciudad[i][j]
                lista.append(mineral)
            if(n+1 == i and m ==j):
                mineral= a + ciudad[i][j]
                lista.append(mineral)
            if(n == i and m-1 == j):
                mineral= a + ciudad[i][j]
                lista.append(mineral)
            if(n == i and m+1 == j):
                mineral= a + ciudad[i][j]
                lista.append(mineral)
            mineral=maximo(lista)
    return mineral
def max_mineral(ciudad):
    l=[]
    for n in range(len(ciudad)):
        for m in range(len(ciudad[0])):
            a=mineral_extraido(ciudad,n,m)
            l.append(a)
            max=maximo(l)
    return max

ciudad=[[30, 80, 50, 10], 
       [50, 20, 90, 30], 
       [20, 30, 10, 90], 
       [20, 80, 80, 20]]
print("la mayor cantidad de mineral extraido es:",max_mineral(ciudad))
