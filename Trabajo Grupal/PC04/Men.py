import matricula
import cancelar_matricula
import registro_de_alumnos
import cursos

def mostrar_menu():
    m = None
    while (m != 5):
        print(" 1:Matricula.")
        print(" 2:Cancelar matricula.")
        print(" 3:Registro de alumnos.")
        print(" 4:Cursos.")
        print(" 5:Salir.")
        m=int(input("Por favor elija una opcion: "))
        if (m == 1):
            matricula.matricula()
        elif (m == 2):
            cancelar_matricula.cancelar()
        elif (m == 3):
            registro_de_alumnos.alumno()
        elif (m == 4):
            cursos.cursos_asignados()
        elif (m == 5):
            break
        else:
            print("Ingrese una opcion correcta")

mostrar_menu()