def Val_pos_neg (L):
    if len(L) > 0 :
        if L[len(L) - 1] > 0 :
            L.pop()
            N = Val_pos_neg(L)
            return N
        else :
            return "false"
    else :
            return "true"
        
L = [7, 10, 0, 15]

Val_pos_neg(L)

if (Val_pos_neg(L) == "true") :
    print("Todos los valores son positivos")
else :
    print("No todos los valores son positivos")