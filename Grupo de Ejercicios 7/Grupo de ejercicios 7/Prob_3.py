def C(n,m):
    if (m == 0 or m == n):
        return 1
    else:
        return C(n-1, m-1) + C(n-1, m)

def main():
    print(C(7,3))
    print(C(15,2))
    print(C(19,5))

main()